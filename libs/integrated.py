import os
import time
import requests
import json
from libs.chaves import Chaves
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class Integrated:

    def __init__(self):
        self.path = os.getcwd()
        self.chaves = Chaves()
        self.condor_auth = self.chaves.condor_url
        self.condor_url_auth = self.chaves.condor_url_auth
        self.condor_url = self.chaves.condor_url
        self.condor_api = self.chaves.condor_api

    # Auth Intranet do Condor
    def condor_connect(self):
        client = 'client_id=web_app'
        usuario = 'username=12055&password=12055'
        payload = '%s&grant_type=password&%s' % (client, usuario)
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
        }
        res = requests.request(
            'POST',
            self.condor_url_auth,
            data=payload,
            headers=headers,
            verify=False
        )
        token = json.loads(res.text)
        return token['access_token']

    # GET ALL Campanhas
    def All_Campanha(
            self,
            inicio=time.strftime('%Y-%m-%d'),
            fim=time.strftime('%Y-%m-%d')
    ):
        url = '%scampanhas' % self.condor_api
        headers = dict(ind_plataforma='SITE', dta_vigencia_inicio=inicio, dta_vigencia_fim=fim,
                       ind_situacao_campanha='CONVERTIDA', Authorization='Bearer ' + self.condor_connect())
        res = requests.request(
            'GET',
            url,
            headers=headers,
            verify=False
        )
        output = json.loads(res.text)
        if res.status_code == 200:
            return output
        elif res.status_code != 200:
            return output

    # GET ID Campanha
    def Id_Campanha(
            self,
            cod_campanha,
            inicio=time.strftime('%Y-%m-%d'),
            fim=time.strftime('%Y-%m-%d')
    ):
        print(inicio)
        print(fim)
        print(cod_campanha)
        url = '%scampanhas' % self.condor_api
        _code = '%s' % cod_campanha
        headers = dict(ind_plataforma='SITE', dta_vigencia_inicio=inicio, dta_vigencia_fim=fim,
                       ind_situacao_campanha='CONVERTIDA', cod_campanha='%s' % _code,
                       Authorization='Bearer ' + self.condor_connect())
        print(headers)
        res = requests.request(
            'GET',
            url,
            headers=headers,
            verify=False
        )
        output = json.loads(res.text)
        if res.status_code == 200:
            return output
        elif res.status_code != 200:
            return output

    # GET ALL Produtos da uma campanha
    def All_Produtos_Campanha(
            self,
            cod_campanha
    ):
        url = '%scampanhas/detalhar/%s' % (self.condor_api, cod_campanha)
        headers = {
            'Authorization': 'Bearer ' + self.condor_connect()
        }
        res = requests.request(
            'GET',
            url,
            headers=headers,
            verify=False
        )
        output = json.loads(res.text)
        if res.status_code == 200:
            return output
        elif res.status_code != 200:
            return output

    # SAVE Imagem do Produto na pasta images
    def All_Image(
            self,
            cod_produto
    ):
        url = '%simagem/%s' % (
            self.condor_api, cod_produto)
        headers = {
            'Authorization': 'Bearer ' + self.condor_connect()
        }
        res = requests.request(
            'GET',
            url,
            headers=headers,
            verify=False
        )
        code = 'Ok'
        if res.status_code == 200:
            if res.content:
                with open('%s/public/images/%s.png' % (self.path, cod_produto), 'wb') as f:
                    f.write(res.content)
                return code
