import requests
import json
from libs.chaves import Chaves
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class Marketing:

    def __init__(self):
        self.chaves = Chaves()
        self._url = self.chaves.mkt_url
        self._token = '%s%s' % (self.chaves.mkt_A, self.chaves.mkt_B)

    # GET ALL
    def Get_All(self, table):
        url = '%s/%s?access_token=%s' % (
            self._url, table, self._token
        )
        res = requests.request(
            'GET',
            url,
            verify=False
        )
        if res.status_code == 200:
            output = json.loads(res.text)
            return output

    # GET Pesquisa Campanha
    def Get_Campanha(self, code_campanha):
        filtro = 'filter[where][cod_campanha]=%s' % code_campanha
        token = 'access_token=%s' % self._token
        url = '%s/Campanhas?%s&%s' % (
            self._url, token, filtro
        )
        res = requests.request(
            'GET',
            url,
            verify=False
        )
        if res.status_code == 200:
            output = json.loads(res.text)
            return output

    # GET Id registro do MySQL da campanha
    def Get_Campanha_Id(self, code_campanha):
        filtro = 'filter[where][codigo]=%s' % code_campanha
        token = 'access_token=%s' % self._token
        url = '%s/Campanhas?%s&%s' % (
            self._url, token, filtro
        )
        res = requests.request(
            'GET',
            url,
            verify=False
        )
        output = json.loads(res.text)
        if res.status_code == 200:
            print('Get %s' % output)
            return output[0]['id']
        elif res.status_code == 404:
            assert isinstance(output, object)
            print(output)

    # GET Pesquisa Produto
    def Get_Produto(self, code_produto):
        token = 'access_token=%s' % self._token
        url = '%s/Produtos/%s?%s' % (
            self._url, code_produto, token
        )
        res = requests.request(
            'GET',
            url,
            verify=False
        )
        output = json.loads(res.text)
        if res.status_code == 200:
            return output
        elif res.status_code != 200:
            return output

    def Get_Produto_Code(
            self, code_produto):
        token = 'access_token=%s' % self._token
        url = '%s/Produtos?filter[where][cod_produto]=%s&%s' % (
            self._url, code_produto, token
        )
        res = requests.request(
            'GET',
            url,
            verify=False
        )
        output = json.loads(res.text)
        if res.status_code == 200:
            return output
        elif res.status_code != 200:
            print(output)

    # POST para API Marketing
    # def Send(self, Url, table, data):
    #     url = '%s/%s' % (
    #         Url, table
    #     )
    #     res = requests.request(
    #         'POST',
    #         url,
    #         data=data
    #     )
    #     if res.status_code == 200:
    #         output = json.loads(res.text)
    #         print(output)
    #         return output
    #     elif res.status_code != 200:
    #         print(json.loads(res.text))
    #         return json.loads(res.text)

    def Send(self, table, data):
        url = '%s/%s?access_token=%s' % (
            self._url, table, self._token
        )
        res = requests.request(
            'POST',
            url,
            data=data,
            verify=False
        )
        if res.status_code == 200:
            output = json.loads(res.text)
            return output
        elif res.status_code != 200:
            return json.loads(res.text)


    # Update Produto Kit
    def UpdateKit(self, codigo, kit):
        url = '%s/Produtos/updateKit' % self._url
        data = {
            'id': codigo,
            'kit': kit
        }
        res = requests.request(
            'POST',
            url,
            data=data,
            verify=False
        )
        if res.status_code == 200:
            output = json.loads(res.text)
            print(output)
            return output
        elif res.status_code != 200:
            print(json.loads(res.text))

    # Delete Database
    def Delete(self, table, Id):
        url = '%s/%s/%s?access_token=%s' % (
            self._url, table, Id, self._token
        )
        res = requests.request(
            'DELETE',
            url,
            verify=False
        )
        if res.status_code == 200:
            output = json.loads(res.text)
            print(output)
            return output

    def CountProduto(self, cod_produto):
        token = 'access_token=%s' % self._token
        url = '%s/Produtos/count?[where][cod_produto]=%s&%s' % (
            self._url, cod_produto, token
        )
        res = requests.request(
            'GET',
            url,
            verify=False
        )
        output = json.loads(res.text)
        if res.status_code == 200:
            return output['count']
        elif res.status_code != 200:
            print(output)

    def GetProductCode(self, url, cod_produto):
        _url = '%s/Products?filter[where][host]=%s' % (url, cod_produto)
        res = requests.request('GET', _url)
        output = json.loads(res.text)
        if res.status_code == 200:
            return output
        elif res.status_code != 200:
            print(output)

    def GetCountProduto(self, url, cod_produto):
        _url = '%s/Products/count?[where][host]=%s' % (url, cod_produto)
        res = requests.request('GET', _url)
        output = json.loads(res.text)
        if res.status_code == 200:
            return output['count']
        elif res.status_code != 200:
            print(output)

    def UpdateProductKit(self, url, codigo, kit):
        _url = '%s/Products/updateKit' % url
        data = {
            'id': codigo,
            'kit': kit
        }
        res = requests.request('POST', _url, data=data)
        if res.status_code == 200:
            output = json.loads(res.text)
            print(output)
            return output
        elif res.status_code != 200:
            print(json.loads(res.text))

    def UpdateProductPhoto(self, url, codigo):
        _url = '%s/Products/updatePhoto' % url
        data = {
            'host': codigo,
            'photo': 1
        }
        res = requests.request('POST', _url, data=data)
        if res.status_code == 200:
            output = json.loads(res.text)
            print(output)
            return output
        elif res.status_code != 200:
            print(json.loads(res.text))


    def CountProdutoXCampanha(self, url, campanha):
        token = 'access_token=%s' % self._token
        _url = '%s/Products/count?[where][campanha]=%s' % (url, campanha)
        res = requests.request('GET', url)
        output = json.loads(res.text)
        if res.status_code == 200:
            return output['count']
        elif res.status_code != 200:
            print(output)