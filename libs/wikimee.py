import requests
import xml.etree.ElementTree as ET
import xmltodict
import pprint
import json


class Wikimee:
    def __init__(self):
        self._url = 'https://wikimee.condor.com.br/catalog/service'

    def connect(self):
        url = "https://wikimee.condor.com.br/catalog/service/produto"

        payload = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:hs=\"http://wikimee.condor.com.br/catalog/service/produto?wsdl\">\n\t<soapenv:Body>\n\t\t<produtos>\n\t\t\t<page>1</page>\n\t\t\t<post>\n\t\t\t\t<options>\n\t\t\t\t\t<sku>7891150008892</sku>\n\t\t\t\t</options>\n\t\t\t</post>\n\t\t</produtos>\n\t</soapenv:Body>\n</soapenv:Envelope>"
        headers = {
            'Content-Type': "text/xml",
            'Authorization': "Basic c3Vwb3J0ZTpzdXBvcnRlMjAxMQ==",
            'User-Agent': "PostmanRuntime/7.19.0",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Host': "wikimee.condor.com.br",
            'Accept-Encoding': "gzip, deflate",
            'Content-Length': "323",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("POST", url, data=payload, headers=headers, verify=False)
        # print(response.text)
        doc = xmltodict.parse(response.text, process_namespaces=True)
        pp = pprint.PrettyPrinter(indent=4)
        # pp.pprint(json.dumps(doc))
        for out in json.loads(doc):
            pprint(out)
        ## pp.pprint(my_dict['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:produtosResponse']['return']['item'][5])
        # pp.pprint(json.dumps(response.text))

