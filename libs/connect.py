#!/usr/bin/env python3
import json
import os
import shutil
import xlsxwriter
import time
from datetime import date
from PIL import Image
from .integrated import Integrated
from .marketing import Marketing
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class Connect:

    def __init__(self):
        self.path = os.getcwd()
        self.mkt = Marketing()
        self.igd = Integrated()

    # Add Dados da Campanha na API Marketing
    def Add_Campanha(self, code):
        camp = self.igd.Id_Campanha(code)
        print(camp)
        data = {
            'codigo': camp[0]['cod_campanha'],
            'nome': camp[0]['nom_campanha'],
            'description': 'string',
            'vigencia_inicio': camp[0]['dta_vigencia_inicio'],
            'vigencia_fin': camp[0]['dta_vigencia_fim'],
            'status': 0,
            'c_at': date.today()
        }
        print(data)
        self.mkt.Send('Campanhas', data)

    # Add Dados do Produto na API Marketing
    def Add_Produto(self, code_campanha, ID):
        global data
        # link de coneccion a la API
        _url = 'Campanhas/%s/produtos' % ID
        # camino y nombre del archivo excel
        xls = '%s/public/relatorio/camp_%s.xls' % (self.path, code_campanha)
        planinha = xlsxwriter.Workbook(xls)
        ws = planinha.add_worksheet()
        ws.write('A1', 'COD_PRODUTO')
        ws.write('B1', 'NOM_PRODUTO')
        ws.write('C1', 'EAN')
        ws.write('D1', 'TAMANHO_IMAGE')
        ws.write('E1', 'COD_DEPARTAMENTO')
        ws.write('F1', 'NOM_DEPARTAMENTO')
        ws.write('G1', 'COD_SETOR')
        ws.write('H1', 'NOM_SETOR')
        row = 1
        col = 0
        camp = self.igd.All_Produtos_Campanha(code_campanha)
        print("Total do produtos a procesar pra está campanha %s" % len(camp))
        # Inicio el Recorrido por cada Producto
        for out in camp['lst_produto']:
            if out['nom_produto'] is None:
                dsc_produto = out['dsc_produto']
            else:
                dsc_produto = out['nom_produto']
            if out['dsc_detalhe_produto'] is None:
                dsc_descricao = out['dsc_folheto']
            else:
                dsc_descricao = out['dsc_detalhe_produto']
            ws.write(row, col, out['cod_produto'])
            ws.write(row, col + 1, out['dsc_produto'])
            # Relatorio de EAN
            for ean in out['lst_ean']:
                ws.write(row, col + 2, ean)
            # Obtener los Datos de Mercadologico
            for i in range(len(out['lst_mercadologico_web'])):
                ws.write(row, col + 4, out['lst_mercadologico_web'][i]['departamento']['cod_mercadologico'])
                ws.write(row, col + 5, out['lst_mercadologico_web'][i]['departamento']['dsc_mercadologico'])
                ws.write(row, col + 6, out['lst_mercadologico_web'][i]['setor']['cod_mercadologico'])
                ws.write(row, col + 7, out['lst_mercadologico_web'][i]['setor']['dsc_mercadologico'])
                # construir el Array para enviar la info a la API
                data = dict(cod_campanha=code_campanha, cod_produto=out['cod_produto'], dsc_produto=dsc_produto,
                            departamento=out['lst_mercadologico_web'][i]['departamento']['cod_mercadologico'],
                            dsc_departamento=out['lst_mercadologico_web'][i]['departamento']['dsc_mercadologico'],
                            setor=out['lst_mercadologico_web'][i]['setor']['cod_mercadologico'],
                            dsc_setor=out['lst_mercadologico_web'][i]['setor']['dsc_mercadologico'],
                            data=json.dumps(out), image=out['cod_produto'], dsc_descricao=dsc_descricao,
                            c_at=date.today())

                if self.verificaImagem(out['cod_produto']):
                    print('Imagen no servidor => %s' % out['cod_produto'])
                    self.mkt.Send(_url, data)
                else:
                    if self.igd.All_Image(out['cod_produto']):
                        count = self.mkt.CountProduto(out['cod_produto'])
                        if count == 0:
                            print('Produto %s pronto para ser cadastrado' % out['cod_produto'])
                            self.mkt.Send(_url, data)
                        elif count > 0:
                            print('Produto %s duplicado N= %s' % (out['cod_produto'], count))
                        print('Success register product %s' % out['cod_produto'])
                    elif self.igd.All_Image(out['cod_produto']) != 'Ok':
                        print('Produto sem Image => %s' % out['cod_produto'])
            row += 1
        planinha.close()

    def convertImage(self):
        ruta = '%s/public/images/' % self.path
        if os.path.exists('%sexport' % ruta):
            shutil.rmtree('%sexport' % ruta)
        os.mkdir('%sexport' % ruta)
        nova_ruta = '%s/public/images/export/' % self.path
        dirs = os.listdir(ruta)
        for item in dirs:
            if os.path.isfile(ruta + item):
                im = Image.open(ruta + item)
                f, e = os.path.splitext(item)
                imresize = im.resize((200, 200), Image.ANTIALIAS)
                imresize.save(nova_ruta + f + '.jpg', 'JPEG', quality=90)

    def convertImageWikimee(self):
        ruta = '%s/public/wikimee/' % self.path
        # os.mkdir('%sexport' % ruta)
        nova_ruta = '%s/public/export/' % self.path
        dirs = os.listdir(ruta)
        for item in dirs:
            if os.path.isfile(ruta + item):
                im = Image.open(ruta + item)
                f, e = os.path.splitext(item)
                if f.count('_') > 2:
                    print(item)
                else:
                    a, b, c = f.split('_')
                    if b:
                        imresize = im.resize((200, 200), Image.ANTIALIAS)
                        imresize.save(nova_ruta + b + '.png', 'PNG', quality=90)
                        os.rename(ruta + item, ruta + f + '.png')

    def removeFolderImage(self):
        folder = '%s/public/images/' % self.path
        dirs = os.listdir(folder)
        for the_file in dirs:
            file_path = os.path.join(folder, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception as e:
                print(e)

    def verificaImagem(self, product):
        produtos = str(product)
        code = 'ok'
        folder = '%s/../../server/server/files/produtos/' % self.path
        dirs = os.listdir(folder)
        for the_file in dirs:
            f, e = os.path.splitext(the_file)
            if f == produtos:
                return code

    def updateKitProduto(self, code_campanha, ID):
        _url = 'Campanhas/%s/produtos' % ID
        camp = self.igd.All_Produtos_Campanha(code_campanha)
        for kit in camp['lst_kit']:
            a, b = kit['dsc_kit'].split('-')
            print('######## Last')
            print(b)
            print('######## Replace')
            novo = b.replace('\\', '').replace(' ,', ',').replace(' .', '.')
            print(novo)
            for out in kit['lst_produto']:
                product = self.mkt.Get_Produto_Code(out['cod_produto'])
                if product:
                    for doug in product:
                        print(doug['id'])
                        self.mkt.UpdateKit(doug['id'], novo)
