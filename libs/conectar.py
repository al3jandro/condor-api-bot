#!/usr/bin/env python3
import json
import os
import shutil
import xlsxwriter
import time
from datetime import date
from PIL import Image
from .integrated import Integrated
from .marketing import Marketing
import urllib3


class Conectar:
    global data

    def __init__(self):
        self.path = os.getcwd()
        self.igd = Integrated()
        self.mkt = Marketing()
        self._url = 'http://localhost:3030/api'

    def GetCampanha(self):
        camp = self.igd.All_Campanha()

        for out in camp:
            print('Campanha: %s' % out['cod_campanha'])
            print('Start: %s' % out['dta_vigencia_inicio'])
            print('End: %s' % out['dta_vigencia_fim'])
            self.AddProduct(out['cod_campanha'], out['dta_vigencia_inicio'], out['dta_vigencia_fim'])
            self.UpdateProductKit(out['cod_campanha'])
            self.AddPrecoXLoja(out['cod_campanha'])
            self.ImagemCampanha(out['cod_campanha'])

    def AddProduct(self, campanha, start, end):
        print('Init Exportando dados a Tabela do Product na API Marketing')
        prod = self.igd.All_Produtos_Campanha(campanha)
        if prod['lst_produto'] is not None:
            for out2 in prod['lst_produto']:
                for i in range(len(out2['lst_mercadologico_web'])):
                    data = dict(
                        host=out2['cod_produto'], campanha=campanha,
                        start=start, end=end,
                        dsc_produto=out2['nom_produto'], dsc_descricao=out2['dsc_folheto'],
                        departamento=out2['lst_mercadologico_web'][i]['departamento']['cod_mercadologico'],
                        dsc_departamento=out2['lst_mercadologico_web'][i]['departamento']['dsc_mercadologico'],
                        setor=out2['lst_mercadologico_web'][i]['setor']['cod_mercadologico'],
                        dsc_setor=out2['lst_mercadologico_web'][i]['setor']['dsc_mercadologico'],
                        embalagem=out2['embalagem_venda']['dsc_unidade_venda']
                    )
                    count = self.mkt.GetCountProduto(self._url, out2['cod_produto'])
                    if count == 0:
                        self.mkt.Send(self._url, 'Products', data)
                    else:
                        print('Produto já Cadastrado')
        count_product = self.mkt.CountProdutoXCampanha(self._url, campanha)
        print('Campanha %s. Inserido %s Produtos' % (campanha, count_product))

    def UpdateProductKit(self, campanha):
        print('Inserindo a descrição do Kit na promoções')
        prod = self.igd.All_Produtos_Campanha(campanha)
        if prod['lst_kit'] is not None:
            for kit in prod['lst_kit']:
                a, b = kit['dsc_kit'].split('-')
                print('######## Last')
                print(b)
                print('######## Replace')
                novo = b.replace('\\', '').replace(' ,', ',').replace(' .', '.')
                print(novo)
                for out3 in kit['lst_produto']:
                    product = self.mkt.GetProductCode(self._url, out3['cod_produto'])
                    if product:
                        for doug in product:
                            print(doug['id'])
                            self.mkt.UpdateProductKit(self._url, doug['id'], novo)

    def AddPrecoXLoja(self, campanha):
        print('Inseriendo as regras do preço pro Loja')
        prod = self.igd.All_Produtos_Campanha(campanha)
        if prod['lst_produto'] is not None:
            for out2 in prod['lst_produto']:
                if out2['lst_preco_regiao'] is not None:
                    for regiao in out2['lst_preco_regiao']:
                        if regiao['lst_mix_regiao'] is not None:
                            for loja in regiao['lst_mix_regiao']:
                                data = dict(
                                    host_id=out2['cod_produto'],
                                    cod_loja=loja,
                                    preco_regular=regiao['vlr_preco_regular'],
                                    parcela_regular=regiao['vlr_parcela_regular'],
                                    preco_clube=regiao['vlr_preco_clube'],
                                    parcela_clube=regiao['vlr_parcela_clube'],
                                    qtd_clube=regiao['qtd_parcela_clube'],
                                    qtd_parcela=regiao['qtd_parcela_regular'],
                                )
                                # print(data)
                                self.mkt.Send(self._url, 'Precos', data)

    def ImagemCampanha(self):
        print('Trabalhando com as Imagens')
        camp = self.igd.All_Campanha()

        for out in camp:
            prod = self.igd.All_Produtos_Campanha(out['cod_campanha'])
            if prod['lst_produto'] is not None:
                for out2 in prod['lst_produto']:
                    # if self.verificaImagem(out2['cod_produto']):
                    #     print('A Imagem do produto %s' % out2['cod_produto'])
                    # else:
                    if self.igd.All_Image(out2['cod_produto']):
                        print('Imagem %s foi criada' % out2['cod_produto'])
                    elif self.igd.All_Image(out2['cod_produto']) != 'Ok':
                        print('Produto sem Image => %s' % out2['cod_produto'])


    def convertImage(self):
        ruta = '%s/public/images/' % self.path
        if os.path.exists('%sexport' % ruta):
            shutil.rmtree('%sexport' % ruta)
        os.mkdir('%sexport' % ruta)
        nova_ruta = '%s/public/images/export/' % self.path
        dirs = os.listdir(ruta)
        for item in dirs:
            if os.path.isfile(ruta + item):
                im = Image.open(ruta + item)
                f, e = os.path.splitext(item)
                imresize = im.resize((250, 250), Image.ANTIALIAS)
                imresize.save(nova_ruta + f + '.png', 'PNG', quality=90)

    def removeFolderImage(self):
        folder = '%s/public/images/' % self.path
        dirs = os.listdir(folder)
        for the_file in dirs:
            file_path = os.path.join(folder, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception as e:
                print(e)

    def verificaImagem(self, product):
        produtos = str(product)
        code = 'ok'
        folder = '%s/public/banco/' % self.path
        dirs = os.listdir(folder)
        for the_file in dirs:
            f, e = os.path.splitext(the_file)
            if f == produtos:
                return 300


    def addLoja(self):
        prod = self.igd.All_Produtos_Campanha(220138)
        if prod:
            for loja in prod['lst_loja']:
                text = '%s %s' %(loja['nom_loja_prefixo'], loja['nom_loja'])
                data = dict(
                    cod_loja = loja['cod_loja'],
                    nome = text
                )
                self.mkt.Send(self._url, 'Lojas', data)